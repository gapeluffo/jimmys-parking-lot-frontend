/* eslint no-param-reassign: ["error", { "props": false }] */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
  parking: '',
  levels: '',
  currentLevel: '',
};

const actions = {

};

const mutations = {
  setParking(state, { parking }) {
    state.parking = parking;
    state.levels = parking.levels.data;
    state.currentLevel = state.levels[0];
  },
  setCurrentLevel(state, { id }) {
    state.currentLevel = state.levels.find(l => l.id === id);
  },
  setCurrentSpot(state, { spot }) {
    const level = state.levels.find(l => l.id === spot.level.id);
    if (level) {
      const currentSpot = level.spots.data.find(s => s.id === spot.id);
      currentSpot.available = false;
    }
  },
};

export default new Vuex.Store({
  state,
  actions,
  mutations,
});
