import api from './../services/api';

function get() {
  return api.get('parking');
}

function findSpot(data) {
  return api.post('findParkingSlot', data);
}

export default {
  get,
  findSpot,
};

