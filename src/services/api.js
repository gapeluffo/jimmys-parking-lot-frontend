import axios from 'axios';

const BASE_URL = process.env.BASE_URL;

function call(url, method, data = {}) {
  return axios({
    method,
    url: BASE_URL + url,
    data,
  })
    .then(response => response.data.data)
    .catch(error => Promise.reject(error));
}

function get(url) {
  return call(url, 'get');
}

function post(url, data) {
  return call(url, 'post', data);
}

function put(url, data) {
  return call(url, 'put', data);
}

function destroy(url) {
  return call(url, 'delete');
}


export default {
  get,
  post,
  put,
  destroy,
};
