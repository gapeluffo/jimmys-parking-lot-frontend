# # Jimmy's Parking Lot - Frontend

This is a simple project to test and manage the parking lot's available spots.

## Requirements

1.  npm

> Frontend project for Jimmy's Parking Lot. 

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Config files
Configure your local/production api server under the `config/` directory.

``` bash
# Set up the correct API URL
BASE_URL: '"http://<your_api_url>/api/"'
```

## Live Demo
Live demo available at: http://159.65.246.96/#/

## Screenshots
![alt text](screenshots/Main.png "Main page")
![alt text](screenshots/SpotFound.png "SpotFound")
